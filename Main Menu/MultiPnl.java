package MainMenu;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class MultiPnl extends Panels {
    private Image mTitle = Toolkit.getDefaultToolkit().getImage("img/menu/choose game mode.png");
    private Image norm = Toolkit.getDefaultToolkit().getImage("img/menu/normal.png");
    private Image boss = Toolkit.getDefaultToolkit().getImage("img/menu/boss.png");
    public MultiPnl() {
        setFocusable(true);
    }
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        g.drawImage(mTitle,100,100, 600, 100, this);
        g.drawImage(norm,250,400, 250, 100, this);
        g.drawImage(boss, 300,650,200, 100, this);
    }
}
