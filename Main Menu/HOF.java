package MainMenu;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class HOF extends JFrame{
    
    public JPanel pnl=new JPanel();
    HOFPnl hofPnl = new HOFPnl();
    int x,y;
    
    public HOF() {
        this.setTitle("Hall of Fame");
        this.setBounds(Constants.winX,Constants.winY,Constants.W,Constants.H);
        this.setResizable(false);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());
        Panels.repaintT(hofPnl);
        this.add(hofPnl,BorderLayout.CENTER);
        pnl.setLayout(new BorderLayout());
        pnl.setBackground(Color.yellow);
        this.add(pnl,BorderLayout.SOUTH);
        MouseListeners(this);
    }
    private void MouseListeners(JFrame f)
    {
        hofPnl.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseReleased(MouseEvent e)
            {
                x = e.getX();
                y = e.getY();
                if(x>=10&&x<=90&&y>=10&&y<=60)
                {
                    Audio.pew();
                    Frame fr = new Frame();
                    fr.setVisible(true);
                    f.setVisible(false);
                }
            }
        
        });
        
    }
            
    
}
