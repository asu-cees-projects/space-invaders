package MainMenu;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class ModeScrn extends Panels {
    private Image title = Toolkit.getDefaultToolkit().getImage("img/menu/choose mode.png");
    private Image sngl = Toolkit.getDefaultToolkit().getImage("img/menu/single player.png");
    private Image mlti = Toolkit.getDefaultToolkit().getImage("img/menu/multiplayer.png");
    private Image boss = Toolkit.getDefaultToolkit().getImage("img/menu/boss.png");
    private Image home = Toolkit.getDefaultToolkit().getImage("img/menu/H.png");
    public ModeScrn() {
        setFocusable(true);
    }
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        g.drawImage(title,100,63, 600, 75, this);
        g.drawImage(sngl,200,201, 400, 75, this);
        g.drawImage(mlti,200,339,400, 75, this);
        g.drawImage(boss,300,477,200, 60, this);
        g.drawImage(home, 10, 10,80,50 ,this);
    }
}
