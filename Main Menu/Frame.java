package MainMenu;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class Frame extends JFrame{
    private int x,y;
    JFrame GameFrame = new JFrame();
    HomeScr GamePanel = new HomeScr();
    
    
    public Frame() {
        this.setVisible(true);
        this.setTitle("Space Invaders");
        this.setBounds(Constants.winX,Constants.winY,Constants.W,Constants.H);
        this.setResizable(false);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        Panels.repaintT(GamePanel);
        this.add(GamePanel);
        mouseListeners(this);
        
    }
    private void mouseListeners(JFrame f){
        GamePanel.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseReleased(MouseEvent e){
                x = e.getX();
                y = e.getY();
                if(x>=300&&x<=500&&y<=270&&y>=195)
                {
                    Audio.pew();
                    Mode mode1 = new Mode();//play
                    f.setVisible(false);
                    mode1.setVisible(true);
                    
                }
                if(x>=200&&x<=600&&y>=330&&y<=405)
                {
                    Audio.pew();
                    HOF hof = new HOF();//hall of fame
                    f.setVisible(false);
                    hof.setVisible(true);
                }
                if(x>=300&&x<=500&&y>=465&&y<=540)//exit
                {
                    Audio.pew();
                    System.exit(0);
                }
            }
                
            
        });
    }
}
