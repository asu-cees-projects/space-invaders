package MainMenu;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public abstract class Panels extends JPanel {
    public  Image bg = Toolkit.getDefaultToolkit().getImage("img/spacebg.jpeg");
    private static javax.swing.Timer repaint;
     public static void repaintT(JPanel pnl)
    {
        repaint = new Timer(10,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                pnl.repaint();
            }
        });
        repaint.start();
    }
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        g.drawImage(bg, 0,0, this);
    }
    
}
