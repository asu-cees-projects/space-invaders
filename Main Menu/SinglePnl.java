package MainMenu;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class SinglePnl extends Panels {
    private Image mTitle = Toolkit.getDefaultToolkit().getImage("img/menu/choose game mode.png");
    private Image normal = Toolkit.getDefaultToolkit().getImage("img/menu/normal.png");
    private Image boss = Toolkit.getDefaultToolkit().getImage("img/menu/boss.png");
    public SinglePnl() {
        setFocusable(true);
    }
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        g.drawImage(mTitle,100,100, 600, 100, this);
        g.drawImage(normal,250,400, 250, 100, this);
        g.drawImage(boss, 300,650,200, 100, this);
    }
}
