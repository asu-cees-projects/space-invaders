package MainMenu;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
public class HOFPnl extends Panels{
    Image hof = Toolkit.getDefaultToolkit().getImage("img/menu/hall of fame.png");
    Image home = Toolkit.getDefaultToolkit().getImage("img/menu/H.png");
    public HOFPnl() {
        setFocusable(true);
    }
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        g.drawImage(hof,200,50,400,50,this);
        g.drawImage(home, 10, 10,80,50 ,this);
        paintNames(g);
    }
    public void paintNames(Graphics g)
    {
        try{
            FileReader fr = new FileReader("hoffile.txt");
            BufferedReader br = new BufferedReader(fr);
            g.setFont(new Font("Arial",Font.PLAIN,50));
            g.setColor(new Color(107,67,190));
            for(int i =0;i<5;i++)
            {
                g.drawString(br.readLine(), 100, 200+(i*75));
                g.drawString(br.readLine(), 550, 200+(i*75));
            }
        }
        catch(Exception e)
        {
            
        }
        
    }
   }
