package MainMenu;

import java.awt.*;

public class Constants {
    private static Dimension x = Toolkit.getDefaultToolkit().getScreenSize();
    private static double X = x.getWidth();
    private static double Y = x.getHeight();
    public static int W = 800;
    public static int H = 600;
    public static int xGen = (int)(X-W)/2;
    public static int yGen = (int)(Y-H)/2;
    public static int winX = xGen;
    public static int winY = yGen;
    public static final int WIDTH=10;
    public static final int PWIDTH=80;
    public static final int PHEIGHT=60;
    public static final int DESDELAY=80;
    public static final int FIRDELAY=500;
    public static final int maxLevel=9;
    
}
