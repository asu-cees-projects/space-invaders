package MainMenu;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class HomeScr extends Panels {
    private Image title = Toolkit.getDefaultToolkit().getImage("img/menu/space invaders.png");
    private Image play = Toolkit.getDefaultToolkit().getImage("img/menu/play.png");
    private Image exit = Toolkit.getDefaultToolkit().getImage("img/menu/exit.png");
    private Image hof = Toolkit.getDefaultToolkit().getImage("img/menu/hall of fame.png");
   
    public HomeScr() {
     setFocusable(true);
    }
   
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        g.drawImage(title,150,60,500,75,this);
        g.drawImage(play,300,195,200,75,this);
        g.drawImage(hof,200,330,400,75,this);
        g.drawImage(exit,300,465,200,75,this);
    }
}
