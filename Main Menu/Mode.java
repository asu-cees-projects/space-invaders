package MainMenu;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import BossMode.*;
import SinglePlayer.*;
import MultiPlayer.*;
public class Mode extends JFrame { //2
    int x,y;
    ModeScrn modeScrn = new ModeScrn();
    
    public Mode() {
        this.setResizable(false);
        this.setTitle("Choose Mode");
        this.setBounds(Constants.winX,Constants.winY,Constants.W,Constants.H);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        Panels.repaintT(modeScrn);
        this.add(modeScrn);
        mouseListeners(this);
    }
    private void mouseListeners(JFrame f)
    {
        modeScrn.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseReleased(MouseEvent e)
            {
                x = e.getX();
                y = e.getY();
                if(x>=200&&x<=600&&y>=201&&y<=276)
                {
                    Audio.pew();
                    spaceFrame sngl = new spaceFrame();
                    sngl.setVisible(true);
                    f.setVisible(false);
                    
                }
                if(x>=200&&x<=600&&y>=339&&y<=414)
                {
                    Audio.pew();
                    Player2 multi = new Player2();
                    multi.setVisible(true);
                    f.setVisible(false);
                    
                }
                if(x>=300&&x<=500&&y>=477&&y<=537)
                {
                    Audio.pew();
                    f.setVisible(false);
                    BossFrame bf = new BossFrame();
                    bf.setVisible(true);
                    
                }
                if(x>=10&&x<=90&&y>=10&&y<=60)
                {
                    Audio.pew();
                    Frame fr = new Frame();
                    fr.setVisible(true);
                    f.setVisible(false);
                }
                
            }
        });
    }
    
    
}
