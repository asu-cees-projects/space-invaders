
package SinglePlayer;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class Army {
    public ArrayList<Enemy> army=new ArrayList<Enemy>();
    public int size;
    //int speed=200;
    public int i=1;
    int altitude=1;
    public boolean gameover=false;
    public static int numofDes=0;
    public Army()
    {
       size=i*10;
       for(int k=0;k<1;k++)
       {
       for(int j=0;j<10;j++)
        {
            army.add(new Enemy(j*80,k*60));
        }
       }
    }
    public void draw(Graphics g)
    {
        for(int j=0;j<army.size();j++)
        {
            army.get(j).draw(g);
        }
            
    }
    public void descend()
    {
        
        for(int j=0;j<army.size();j++)
        {
            if(!(army.get(j).toLeft))
            {
                if(army.get(j).drop)
                {
                    
                    army.get(j).setY((army.get(j).getY())+60);
                    army.get(j).drop=false;
                    altitude++;
                }
                else if (!(army.get(j).drop))
                {
                    army.get(j).setX((army.get(j).getX())+5);
                }
            }
            else if( army.get(j).toLeft)
            {
                if(army.get(j).drop)
                {
                    
                    army.get(j).setY((army.get(j).getY())+60);
                    army.get(j).drop=false;
                    altitude++;
                }
                else if (!(army.get(j).drop))
                {
                    army.get(j).setX((army.get(j).getX())-5);
                }
            }
            if( !(army.get(j).isDestroyed) && army.get(j).getY()>510)
            {
                gameover=true;
            }
        }
//        if(army.get(0).getY()<665){
//        for(int j=0;j<size;j++)
//        {
//            army.get(j).setY((army.get(j).getY())+5);
//        }}

    }
    public void checkDir()
    {
         for(int j=0;j<army.size();j++){
                 if(army.get(j).getX()<0)
                 {
                     
                     army.get(j).setX(army.get(j).getX()+2);
                     army.get(j).toLeft=false;
                     if(army.get(j).getY()<(60*altitude)+1)
                        army.get(j).drop=true;
                 }
                 else if(army.get(j).getX()>719)
                    {
                        army.get(j).setX(army.get(j).getX()-2);
                        army.get(j).toLeft=true;
                        if(army.get(j).getY()<(60*altitude)+1)
                        army.get(j).drop=true;
                     }
         }
    }
  
}
