
package SinglePlayer;

import SinglePlayer.*;
import MainMenu.*;
import BossMode.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class spacePanel extends Panels {
    Fighter fighter;
    Army army;
    FighterAmmo fa;
    EnemyAmmo ea;
    public spacePanel(Fighter f,Army a, FighterAmmo fa,EnemyAmmo ea)
    {
        this.fighter=f;
        this.army=a;
        this.fa=fa;
        this.setBackground(Color.WHITE);
        this.ea = ea;
    }
    public void paint(Graphics g)
    {
        super.paint(g);
        fighter.paint(g);
        army.draw(g);
        fa.paintfa(g);
        ea.paintEp(g);
        
    }
//    @Override
//  protected void paintComponent(Graphics g) {
//
//      Image bg=Toolkit.getDefaultToolkit().getImage("img/spacebg.jpeg");
//      super.paintComponent(g);
//        g.drawImage(bg, 0, 0, null);
//}
}
