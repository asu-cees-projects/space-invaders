
package SinglePlayer;

import MultiPlayer.*;
import MainMenu.*;
import BossMode.*;
import spaceinvadersgame.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
import java.util.*;
import sun.audio.*;
import java.io.*;

public class spaceFrame extends JFrame {
    public static int level=1;
    private Fighter fighter= new Fighter((Constants.WIDTH*Constants.PWIDTH/2)-20, (Constants.WIDTH*Constants.PHEIGHT/2)+230);
    private Army army=new Army();    
    private FighterAmmo fightAmmo= new FighterAmmo();
    private EnemyAmmo enemAmmo= new EnemyAmmo(army);
    private spacePanel spcPnl= new spacePanel(fighter,army,fightAmmo,enemAmmo);
    private JPanel scrPnl= new JPanel();
    public static JLabel scrnum=new JLabel("0");
    private JLabel scrlbl=new JLabel("Score : ");
    private JLabel lvlnum=new JLabel("1");
    private JLabel lvllbl=new JLabel("Level : ");
    private boolean lvlUpFlag=false;
    BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
    Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
    cursorImg, new Point(0, 0), "blank cursor");


    public int getLevel() {
        return level;
    }
    public static int score=0;
    private int lvlCounter;
    private javax.swing.Timer descendTimer;
    private javax.swing.Timer repaintTimer;
    private javax.swing.Timer fightAmmoTimer;
    private javax.swing.Timer enFireTimer; 
    private javax.swing.Timer lvlchkTimer;
    private javax.swing.Timer themeTimer;
    
    public spaceFrame()
    {
//        try{
//            
//        }
//        catch(Exception e){}
        theme();
        this.setTitle("Space Invaders Single Player");
        this.setResizable(false);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setBounds(Constants.winX,Constants.winY,Constants.W,Constants.H);
        spcPnl.setPreferredSize(new Dimension(Constants.WIDTH*Constants.PWIDTH,Constants.WIDTH*Constants.PHEIGHT));
        scrPnl.add(scrlbl);        
        scrPnl.add(scrnum);
        scrPnl.add(lvllbl);
        scrPnl.add(lvlnum);
        this.getContentPane().setCursor(blankCursor);
        Container c=this.getContentPane();
        c.add(spcPnl);
        c.add(scrPnl,BorderLayout.SOUTH);
//        lvlchkTimer= new javax.swing.Timer(1000, new ActionListener(){
//            @Override
//            public void actionPerformed(ActionEvent ae) {
//                lvlUP();
//            }
//            
//        });
//        lvlchkTimer.start();
        themeTimer = new javax.swing.Timer(39000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                theme();
            }
        });
        themeTimer.start();
        repaintTimer = new javax.swing.Timer(10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                spcPnl.repaint();
                //army.draw(getGraphics());
                efhascollision(Fighter.getX(), Fighter.getY());
                fpehascollision();
                epfhasCollision();
//                if (lvlUpFlag)
//                {
//                    lvlUpFlag=false;
//                    spawnarmy();
//                    
//                }
             if(army.gameover)
             {
                 repaintTimer.stop();
                 gameoverSound();
                 ExitPanel ex=new ExitPanel();
                 ex.setVisible(true);
             }
                lvlUP();
                spawnWave();
            }
        });
        repaintTimer.start();
        descendTimer=new javax.swing.Timer(Constants.DESDELAY, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                army.checkDir();
                army.descend();
            }
        });
        descendTimer.start();
        fightAmmoTimer=new javax.swing.Timer(10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                fightAmmo.ascend();
                enemAmmo.drop();
            }
        });
        fightAmmoTimer.start();
        
        enFireTimer= new javax.swing.Timer(Constants.FIRDELAY, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
               Random random = new Random();
                int xf =random.nextInt(army.size);
                //int yf =;
                while(spcPnl.army.army.get(xf).isDestroyed)
                {
                    xf=random.nextInt(army.army.size());
                    if (lvlUP())
                    {
                       
                       break;
                    }
//                    if(!lvlUpFlag)
//                    {
//                        lvlUP();
//                    }
//                    if(lvlUpFlag)
//                    {
//                        //spawnWave();
//                        lvlUpFlag=
//                        break;
//                    } 
                        break;
                }
//                if(lvlUpFlag)
//                {
//                    spawnWave();
//                }
                  
                if(!(spcPnl.army.army.get(xf).isDestroyed))
                    enemAmmo.EnFire(xf, xf);
            }
        });
        enFireTimer.start();
        /*spcPnl.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if(e.getKeyCode()==KeyEvent.VK_UP)
                {
                    if(fighter.getY()>0)
                        fighter.setY(fighter.getY()-Constants.WIDTH);
                }
                else if(e.getKeyCode()==KeyEvent.VK_DOWN)
                {
                    if(fighter.getY()<(Constants.WIDTH*Constants.PHEIGHT)-140)
                        fighter.setY(fighter.getY()+Constants.WIDTH);                    
                }
                else if(e.getKeyCode()==KeyEvent.VK_RIGHT)
                {
                    if(fighter.getX()<(Constants.WIDTH*Constants.PWIDTH)-95)    
                        fighter.setX(fighter.getX()+Constants.WIDTH);                    
                }
                else if(e.getKeyCode()==KeyEvent.VK_LEFT)
                {
                    if(fighter.getX()>0)
                        fighter.setX(fighter.getX()-Constants.WIDTH);                    
                }
                else if(e.getKeyCode()==KeyEvent.VK_SPACE)
                {
                    fightAmmo.fire();
                }
            }
});*/
        spcPnl.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent me) {
                super.mouseMoved(me);
                Fighter.x=me.getX()-40;//To change body of generated methods, choose Tools | Templates.
                Fighter.y=me.getY();//To change body of generated methods, choose Tools | Templates.
            }
            
});
        spcPnl.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                fightAmmo.fire();
                pew();
                
            }
            
});
        
        spcPnl.setFocusable(true);
    }
    
  //enemy fighter collision check 
  public void efhascollision(int x , int y )
  {
    for (int i = 0 ;i<army.army.size();i++)
    {
        if (x>(army.army.get(i).getX())-80 && x<(army.army.get(i).getX())+80 && y>(army.army.get(i).getY())-60 && y<(army.army.get(i).getY())+60)
        {
            if(!(army.army.get(i).isDestroyed))
            {
                gameoverSound();
                //JOptionPane.showMessageDialog(null, "Game Over");
                repaintTimer.stop();
                ExitPanel ex=new ExitPanel();
                ex.setVisible(true);
                this.setVisible(false);
            }
        }
    }
  }
  
  //fighter's projectile and enemy collision 
  public void fpehascollision()
  {
      for (int i = 0 ;i< fightAmmo.af.size();i++)
      {
          for (int j =0;j<army.army.size();j++ )
          {
              if (fightAmmo.af.get(i).getX() >(army.army.get(j).getX()) && fightAmmo.af.get(i).getX() <(army.army.get(j).getX())+80 && fightAmmo.af.get(i).getY() >(army.army.get(j).getY()) &&  fightAmmo.af.get(i).getY() <(army.army.get(j).getY())+60)
              {
                  if(!(army.army.get(j).isDestroyed) && !(fightAmmo.af.get(i).isDestroyed)){
                  army.army.get(j).erase(spcPnl.getGraphics());
                  fightAmmo.af.get(i).erase(spcPnl.getGraphics());}
              }
          }
      }
      
  }
  public void epfhasCollision()
  {
      for(int i=0;i<enemAmmo.ep.size();i++)
      {
          if(enemAmmo.ep.get(i).getX()>Fighter.x-20 && (enemAmmo.ep.get(i).getX()) < Fighter.x+60 && enemAmmo.ep.get(i).getY()> (Fighter.y)-10 && (enemAmmo.ep.get(i).getY())+ 30 <(Fighter.y)+60)
          {
              gameoverSound();
              //JOptionPane.showMessageDialog(null, "Game Over");
              repaintTimer.stop();
              ExitPanel ex=new ExitPanel();
              ex.setVisible(true);
              this.setVisible(false);
          }
      }
  }
  public boolean lvlUP()
  {
      
          if (army.army.size()!=Army.numofDes)
          {
              lvlUpFlag=false;
              return false;
          }
          else{
                lvlUpFlag=true;
                return true;
          }
  }
    public void spawnWave()
    {
        if(lvlUpFlag)
        {
            lvlUpFlag=false;
            lvlSound();
            level++;
            descendTimer.setDelay(Math.max(Constants.DESDELAY-(15*(level-1)),10));
            enFireTimer.setDelay(Math.max(Constants.FIRDELAY-(150*(level-1)),50));
            lvlnum.setText(""+level);
            for(int i=0;i<Math.min(level,4);i++){
                
                for(int j=0;j<10;j++)
                 {
                      army.army.add(new Enemy(j*80,60*i));
                      army.army.get(j).draw(spcPnl.getGraphics());
                    }
                 }
        }
    }
    public static void theme()
    {

        InputStream IS;
        try{
            IS=new FileInputStream(new File("audio/imperial_march.wav"));
            AudioStream AS = new AudioStream(IS);
            AudioPlayer.player.start(AS);
        }
        catch(Exception e){}
    }
     
    public static void pew()
    {

        InputStream IS;
        try{
            IS=new FileInputStream(new File("audio/pew3.wav"));
            AudioStream AS = new AudioStream(IS);
            AudioPlayer.player.start(AS);
        }
        catch(Exception e){}
    }
    public static void lvlSound()
    {

        InputStream IS;
        try{
            IS=new FileInputStream(new File("audio/lvlup3.wav"));
            AudioStream AS = new AudioStream(IS);
            AudioPlayer.player.start(AS);
        }
        catch(Exception e){}
    }
    public static void gameoverSound()
    {

        InputStream IS;
        try{
            IS=new FileInputStream(new File("audio/gameover.wav"));
            AudioStream AS = new AudioStream(IS);
            AudioPlayer.player.start(AS);
        }
        catch(Exception e){}
    }
//  public void spawnarmy()
//  {
//      army = new Army();
//      
//  }
}


