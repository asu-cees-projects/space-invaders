
package SinglePlayer;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class EnemyProjectile {
    //fields
    private Image ep = Toolkit.getDefaultToolkit().getImage("img/game/enemyProjectile-30.png");
    private int x;
    private int y;
    //getters
    public int getX()
    {
        return x;
    }
    public int getY()
    {
        return y;
    }
    //setters
    public void setX(int x)
    {
        this.x = x;
    }
    public void setY(int y)
    {
        this.y = y;
    }

    //constructor
    public EnemyProjectile(int x ,int y) 
    {
        this.x= x+25;
        this.y=y+80;
    }
    //painter
    public void painte(Graphics g)
    {
        g.drawImage(ep, x, y, 30,30,null);
    }
}
