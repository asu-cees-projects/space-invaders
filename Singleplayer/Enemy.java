
package SinglePlayer;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
//import sun.java2d.pipe.DrawImage;

public class Enemy {
    private Image im=Toolkit.getDefaultToolkit().getImage("img/game/Enemy-80x60.png");
    private int x;
    private int y;
    public boolean isDestroyed=false;
    public boolean toLeft=false;
    public boolean drop=false;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public  int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    public Enemy(int x,int y)
    {
        this.x=x;
        this.y=y;
        isDestroyed=false;
        if((y/60)%2==0)
            toLeft=false;
        else
            toLeft=true;
        drop=false;
    }
    public void draw(Graphics g)
    {
        g.drawImage(im, x, y, 80, 60, null);
    }
    public void erase(Graphics g)
    {
        this.im=null;
        this.isDestroyed=true;
        Army.numofDes++;
        spaceFrame.score+=10*(10*spaceFrame.level);
        spaceFrame.scrnum.setText(""+spaceFrame.score);
    }
}
