
package SinglePlayer;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class FighterProjectile
{
    //private Image im=Toolkit.getDefaultToolkit().getImage("test\\programming project\\fighter-80x80.png");
    
    //image 
     public Image im = Toolkit.getDefaultToolkit().getImage("img/game/fighterProjectile-30.png");
    
    //fields 
    private int x ;
    private int y ;
    public boolean isDestroyed=false;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    //constructor 
    public FighterProjectile()
    {
        x = Fighter.getX()+25;
        y = Fighter.getY();       
    }
    
    //paint method 
    public void paintp (Graphics g)
    {
        g.drawImage(im,x,y,30,30,null);   
    }
    public void erase(Graphics g)
    {
        this.im=null;
        this.isDestroyed=true;
    }
}
