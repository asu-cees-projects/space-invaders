
package SinglePlayer;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
//import java.applet.Applet;

public class Fighter /*extends Applet */{
    public Image im=Toolkit.getDefaultToolkit().getImage("img/game/fighter-80x60.png");
     public static int x;
     public static int y;
     public static boolean isDestroyed =false;

    public static int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public static  int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    public Fighter(int x, int y)
    {
        this.x=x;
        this.y=y;        
    }
    public void paint(Graphics g)
    {
        g.drawImage(im, x, y, 80, 60, null);
    }
//    public void fire()
//    {
//        
//    }
}
