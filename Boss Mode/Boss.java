package BossMode;

import java.awt.*;
import java.util.Random;
import SinglePlayer.*;
import MainMenu.*;
import MultiPlayer.*;
public class Boss
{
    //boss image
    Image bim = Toolkit.getDefaultToolkit().getImage("img/game/boss-200x200.png");

    //fields

    //boss coordinates
    public static int bx;
    public static int by;

    //constructor
    public Boss ()
    {
        bx = (Constants.WIDTH*Constants.PWIDTH/4)+100;
        by = (Constants.WIDTH*Constants.PHEIGHT/4)-50;
    }

    //setters
    public static void setBx(int bx)
    {
        Boss.bx = bx;
    }

    public static void setBy(int by)
    {
        Boss.by = by;
    }

    //getters
    public static int getBx()
    {
        return bx;
    }

    public static int getBy()
    {
        return by;
    }

    //methods

    //method to paint boss
    public void paintBoss (Graphics g)
    {
        g.drawImage(bim,bx,by,200,150,null);
    }

    //move method
    public void moveBoss()
    {
        Random rand = new Random();
        int r = rand.nextInt(2);
        int mt = rand.nextInt(15);
        if (Fighter.getY()<Boss.getBy())
        {
            Boss.setBx(Fighter.getX());
            Boss.setBy((Fighter.getY()));
            BossFrame.teleport();
        }
        else
        {
            if (Boss.getBx()>756)
            {
                Boss.setBx(Boss.getBx()-100);
            }
            if(Boss.getBy()>710)
            {
                Boss.setBy(Boss.getBy()-70);
            }
            if (r==0)
            {
                if (Boss.getBx()>48 && Boss.getBy()<510)
                {
                    Boss.setBx(Boss.getBx()-100);
                }
            }
            else if (r==1)
            {
                if (Boss.getBx()<656 && Boss.getBy()<510 )
                {
                    Boss.setBx(Boss.getBx()+100);
                }
            }
            if (mt == 12)
            {
                Boss.setBx(Fighter.getX());
                Boss.setBy(Fighter.getY());
                BossFrame.teleport();
            }

        }
        /*if (r==0)            //move towards fighter
        {
            Boss.setBx(Fighter.getX());
            Boss.setBy(Fighter.getY());
        }
        else if (r==1)       //move left
        {
            if (Boss.getBx()>48 && Boss.getBy()<710)
            {
                Boss.setBx(Boss.getBx()-50);
            }

        }
        else if (r==2)       //move right
        {
            if (Boss.getBx()<759 && Boss.getBy()<710 )
            {
                Boss.setBx(Boss.getBx()+50);
            }

        }*/

    }
}
