package BossMode;

import java.awt.*;
import java.util.ArrayList;
import SinglePlayer.*;
import MainMenu.*;
import MultiPlayer.*;
public class BossAmmo
{
    //array list of boss's projectile
    ArrayList  <BossProjectile> bossammo =new ArrayList<BossProjectile>();

    //methods

    //paint method
    public void paintba(Graphics g)
    {
        for (int i = 0 ;i<bossammo.size();i++)
        {
            bossammo.get(i).paintbp(g);
        }
    }

    //fire method
    public void fireba ()
    {
        for (int i = 0 ;i<bossammo.size();i++)
        {
            bossammo.get(i).setBpy(bossammo.get(i).getBpy()+5);
        }

    }

    //add projectile method
    public void addba ()
    {
        bossammo.add(new BossProjectile());
    }

}
