package BossMode;
import SinglePlayer.*;
import MainMenu.*;
import MultiPlayer.*;
import javax.swing.*;
import java.awt.*;

public class BossPanel extends Panels
{
    //components
    Boss boss ;
    BossAmmo ba ;
    Fighter fighter;
    FighterAmmo fa;

    //constructor
    public BossPanel (Boss boss,BossAmmo ba,Fighter fighter,FighterAmmo fa)
    {
        this.boss=boss;
        this.ba=ba;
        this.fighter=fighter;
        this.fa=fa;
    }

    //methods

    //paint method
    public void paint (Graphics g)
    {
        super.paint(g);
        fighter.paint(g);
        fa.paintfa(g);
        boss.paintBoss(g);
        ba.paintba(g);

    }

    //paint component method
//    @Override
//    protected void paintComponent(Graphics g) {
//
//        Image bg=Toolkit.getDefaultToolkit().getImage("test\\programming project\\spacebg.jpeg");
//        super.paintComponent(g);
//        g.drawImage(bg, 0, 0, null);
//    }

}
