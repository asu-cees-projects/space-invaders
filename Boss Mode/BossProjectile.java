package BossMode;
import SinglePlayer.*;
import MainMenu.*;
import MultiPlayer.*;
import java.awt.*;

public class BossProjectile
{
    //projectile image
    Image bpim = Toolkit.getDefaultToolkit().getImage("img/game/bp2-50x60.png");

    //fields

    //boss projectile coordinates
    private  int bpx;
    private  int bpy;


    //constructor
    public BossProjectile()
    {
        this.bpx=Boss.getBx()+75;
        this.bpy=Boss.getBy()+150;
    }

    //setters
    public void setBpx(int bpx) {
        this.bpx = bpx;
    }

    public void setBpy(int bpy) {
        this.bpy = bpy;
    }

    //getters
    public int getBpx() {
        return bpx;
    }

    public int getBpy() {
        return bpy;
    }


    //methods

    //paint method
    public void paintbp (Graphics g)
    {
        g.drawImage(bpim,bpx,bpy,50,60,null);

    }

}
