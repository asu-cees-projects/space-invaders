package BossMode;

import SinglePlayer.*;
import MainMenu.*;
import MultiPlayer.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

import static java.awt.Color.BLUE;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import static SinglePlayer.spaceFrame.*;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

//import static java.awt.Color.BLACK;

public class BossFrame extends JFrame
{
    //labels
    JLabel fnllvl = new JLabel("Final Level");


    //progress bars
    JProgressBar fhealth = new JProgressBar(0,100);
    JProgressBar bhealth = new JProgressBar(0,100);

    //objects
    private Fighter fighter= new Fighter((Constants.WIDTH*Constants.PWIDTH/2)-20, (Constants.WIDTH*Constants.PHEIGHT/2)+230);    
    FighterAmmo fa = new FighterAmmo();
    Boss boss = new Boss();
    BossAmmo ba = new BossAmmo();

    //panels
    JPanel healthpnl = new JPanel();
    BossPanel bosspnl = new BossPanel(boss,ba,fighter,fa);

    //timers
    private javax.swing.Timer repTimer ;
    private javax.swing.Timer ammoTimer ;
    private javax.swing.Timer bammoTimer ;
    private javax.swing.Timer BossmoveTimer ;
    private javax.swing.Timer collisionTimer ;
    private javax.swing.Timer themeTimer;

    //health value
    public int health =100;
    public int Bhealth =100;

    //hide Cursor
    BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
    Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
            cursorImg, new Point(0, 0), "blank cursor");

    //constructor
    public BossFrame ()
    {
        initiate();
    }

    //methods

    //initilization method
    public void initiate()
    {
        //frame specs
        theme();
        this.setTitle("Space Invaders Boss Level");
        this.setResizable(false);
        this.setDefaultCloseOperation(3);
        this.setBounds(Constants.winX,Constants.winY,Constants.W,Constants.H);

        //boss panel specs
        bosspnl.setPreferredSize(new Dimension(Constants.W,Constants.H-50));

        //setting initial values to progress bars
        fhealth.setValue(100);
        fhealth.setStringPainted(true);
        fhealth.setString("Fighter Health");
        bhealth.setValue(100);
        bhealth.setStringPainted(true);
        bhealth.setString("Boss Health");


        //adding components in panels
        healthpnl.setLayout(new GridLayout(1,3));
        healthpnl.add(fhealth);
        fnllvl.setHorizontalAlignment(0);
        healthpnl.add(fnllvl);
        healthpnl.add(bhealth);


        //Container
        Container cb = this.getContentPane();

        //packing Container with panels
        cb.add(bosspnl,BorderLayout.CENTER);
        cb.add(healthpnl,BorderLayout.SOUTH);

        //cursor specs
        this.getContentPane().setCursor(blankCursor);

        //timers

        //repaint timer
        repTimer = new javax.swing.Timer(10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                bosspnl.repaint();
                fighterIsdead();
                bossIsdead();
            }
        });
        repTimer.start();

        //ammo timer
        ammoTimer = new javax.swing.Timer(10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                fa.ascend();
                ba.fireba();
            }
        });
        ammoTimer.start();

        bammoTimer = new javax.swing.Timer(2000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                ba.addba();
            }
        });
        bammoTimer.start();

        BossmoveTimer = new javax.swing.Timer(350, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                boss.moveBoss();
            }
        });
        BossmoveTimer.start();
        themeTimer = new javax.swing.Timer(39000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                theme();
            }
        });
        themeTimer.start();
        collisionTimer = new javax.swing.Timer(200, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                bafCollision();
                fabCollision();
                bossFighterCollision();
            }
        });
        collisionTimer.start();


        //fighter's motion
        bosspnl.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent me) {
                super.mouseMoved(me);
                Fighter.x=me.getX()-40;//To change body of generated methods, choose Tools | Templates.
                Fighter.y=me.getY();//To change body of generated methods, choose Tools | Templates.
                //System.out.println(""+me.getX()+","+me.getY());
            }

        });
        bosspnl.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                fa.fire();
                pew();

            }

        });

        bosspnl.setFocusable(true);




        this.pack();

    }

    //health check

    //for fighter
    public void fighterIsdead()
    {
        if (fhealth.getValue()==0)
        {
            gameoverSound();
            JOptionPane.showMessageDialog(null, "YOU ARE DEAD!");
            System.exit(0);
        }
    }

    //for boss
    public void bossIsdead()
    {
        if (bhealth.getValue()==0)
        {
            victory();
            JOptionPane.showMessageDialog(null, "YOU WON!");
            System.exit(0);
        }
    }

    //collisions

    //boss's ammo and fighter
    public void bafCollision()
    {
        for (int i = 0 ;i< ba.bossammo.size();i++)
        {
            if (ba.bossammo.get(i).getBpx()>Fighter.x-20 && (ba.bossammo.get(i).getBpx()) < Fighter.x+60 && ba.bossammo.get(i).getBpy()> (Fighter.y)-10 && (ba.bossammo.get(i).getBpy())+ 30 <(Fighter.y)+60)
            {
                health-=10;
                fhealth.setValue(health);
                ba.bossammo.get(i).bpim=null;
            }
        }
    }

    //fighter's ammo and boss
    public void fabCollision()
    {
        for (int i = 0 ;i<fa.af.size();i++)
        {
            if (fa.af.get(i).getX()>boss.getBx()+30 && fa.af.get(i).getX()<boss.getBx()+150 && fa.af.get(i).getY()>boss.getBy()+30 && fa.af.get(i).getY()<boss.getBy()+170)
            {
                Bhealth-=5;
                bhealth.setValue(Bhealth);
                fa.af.get(i).im=null;
            }
        }
    }

    //boss and fighter
    public void bossFighterCollision()
    {
        if (fighter.getX()==boss.getBx() && fighter.getY()==boss.getBy() )
        {
            health-=20;
            fhealth.setValue(health);
        }
    }
    public static void theme()
    {

        InputStream IS;
        try{
            IS=new FileInputStream(new File("audio/imperial_march.wav"));
            AudioStream AS = new AudioStream(IS);
            AudioPlayer.player.start(AS);
        }
        catch(Exception e){}
    }
    public static void pew()
    {

        InputStream IS;
        try{
            IS=new FileInputStream(new File("audio/pew3.wav"));
            AudioStream AS = new AudioStream(IS);
            AudioPlayer.player.start(AS);
        }
        catch(Exception e){}
    }
    public static void lvlSound()
    {

        InputStream IS;
        try{
            IS=new FileInputStream(new File("audio/lvlup3.wav"));
            AudioStream AS = new AudioStream(IS);
            AudioPlayer.player.start(AS);
        }
        catch(Exception e){}
    }
    public static void gameoverSound()
    {

        InputStream IS;
        try{
            IS=new FileInputStream(new File("audio/gameover.wav"));
            AudioStream AS = new AudioStream(IS);
            AudioPlayer.player.start(AS);
        }
        catch(Exception e){}
    }
    public static void victory()
    {

        InputStream IS;
        try{
            IS=new FileInputStream(new File("audio/victory.wav"));
            AudioStream AS = new AudioStream(IS);
            AudioPlayer.player.start(AS);
        }
        catch(Exception e){}
    }
    public static void teleport()
    {

        InputStream IS;
        try{
            IS=new FileInputStream(new File("audio/tel2.wav"));
            AudioStream AS = new AudioStream(IS);
            AudioPlayer.player.start(AS);
        }
        catch(Exception e){}
    }
}
