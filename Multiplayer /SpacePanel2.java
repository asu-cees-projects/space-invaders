
package MultiPlayer;

import SinglePlayer.*;
import MainMenu.*;
import BossMode.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class SpacePanel2 extends Panels {
    Fighter fighter;
    Fighter2 player2;
    Army army;
    FighterAmmo fa;
    FighterAmmo2 fa2;
    EnemyAmmo ea;
    public SpacePanel2(Fighter f,Fighter2 P2,Army a, FighterAmmo fa,FighterAmmo2 fa2,EnemyAmmo ea)
    {
        this.fighter=f;
        this.player2=P2;
        this.army=a;
        this.fa=fa;
        this.fa2=fa2;
        this.setBackground(Color.WHITE);
        this.ea = ea;
    }
    public void paint(Graphics g)
    {
        super.paint(g);
        fighter.paint(g);
        player2.paint(g);
        army.draw(g);
        fa.paintfa(g);
        fa2.paintfa1(g);
        ea.paintEp(g);
        
    }
//    @Override
//  protected void paintComponent(Graphics g) {
//
//      Image bg=Toolkit.getDefaultToolkit().getImage("test\\programming project\\spacebg.jpeg");
//      super.paintComponent(g);
//        g.drawImage(bg, 0, 0, null);
//}
}
