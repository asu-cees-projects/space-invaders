package MultiPlayer;

import SinglePlayer.*;
import MainMenu.*;
import BossMode.*;

import MultiPlayer.*;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.util.Random;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
import java.util.*;
import static SinglePlayer.spaceFrame.theme;

public class Player2 extends JFrame
{
    public static int level=1;
    private Fighter fighter= new Fighter((Constants.WIDTH*Constants.PWIDTH/2)-140, (Constants.WIDTH*Constants.PHEIGHT/2)+230);
    private Fighter2 player2 = new Fighter2((Constants.WIDTH*Constants.PWIDTH/2)+80, (Constants.WIDTH*Constants.PHEIGHT/2)+160);
    private Army army=new Army();    
    private FighterAmmo fightAmmo= new FighterAmmo();
    private FighterAmmo2 fightAmmo2= new FighterAmmo2();
    private EnemyAmmo enemAmmo= new EnemyAmmo(army);
    private SpacePanel2 spcPnl= new SpacePanel2(fighter,player2,army,fightAmmo,fightAmmo2,enemAmmo);
    private JPanel scrPnl= new JPanel();
    //public static JLabel scrnum=new JLabel("0");
    //private JLabel scrlbl=new JLabel("Score : ");
    private JLabel lvlnum=new JLabel("1");
    private JLabel lvllbl=new JLabel("Level : ");
    private boolean lvlUpFlag=false;
    BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
    Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
    cursorImg, new Point(0, 0), "blank cursor");


    public int getLevel() {
        return level;
    }
    //public static int score=0;
    private int lvlCounter;
    private javax.swing.Timer descendTimer;
    private javax.swing.Timer repaintTimer;
    private javax.swing.Timer fightAmmoTimer;
    private javax.swing.Timer enFireTimer; 
    private javax.swing.Timer lvlchkTimer;
    private javax.swing.Timer themeTimer;
    public Player2()
    {
        spaceFrame.theme();
        this.setTitle("Space Invaders Multiplayer");
        this.setResizable(false);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setBounds(Constants.winX,Constants.winY,Constants.W,Constants.H);
        spcPnl.setPreferredSize(new Dimension(Constants.WIDTH*Constants.PWIDTH,Constants.WIDTH*Constants.PHEIGHT));
        //scrPnl.add(scrlbl);        
        //scrPnl.add(scrnum);
        scrPnl.add(lvllbl);
        scrPnl.add(lvlnum);
        this.getContentPane().setCursor(blankCursor);
        Container c=this.getContentPane();
        c.add(spcPnl);
        c.add(scrPnl,BorderLayout.SOUTH);
//        lvlchkTimer= new javax.swing.Timer(1000, new ActionListener(){
//            @Override
//            public void actionPerformed(ActionEvent ae) {
//                lvlUP();
//            }
//            
//        });
//        lvlchkTimer.start();
        repaintTimer = new javax.swing.Timer(10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                spcPnl.repaint();
                //army.draw(getGraphics());
                efhascollision(Fighter.getX(), Fighter.getY());
                efhascollision(Fighter2.getX(), Fighter2.getY());
                fpehascollision();
                fpehascollision2();
                epfhasCollision();
                epfhasCollision2();
                if(player2.isDestroyed==true && fighter.isDestroyed==true)
                {
                    spaceFrame.gameoverSound();
                    JOptionPane.showMessageDialog(null, "Game Over");
                    System.exit(0);
                }
//                if (lvlUpFlag)
//                {
//                    lvlUpFlag=false;
//                    spawnarmy();
//                    
//                }
                lvlUP();
                spawnWave();
            }
        });
        repaintTimer.start();
        themeTimer = new javax.swing.Timer(39000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                spaceFrame.theme();
            }
        });
        themeTimer.start();
        descendTimer=new javax.swing.Timer(Constants.DESDELAY, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                army.checkDir();
                army.descend();
            }
        });
        descendTimer.start();
        fightAmmoTimer=new javax.swing.Timer(10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                fightAmmo2.ascend1();
                fightAmmo.ascend();
                enemAmmo.drop();
            }
        });
        fightAmmoTimer.start();
        
        enFireTimer= new javax.swing.Timer(Constants.FIRDELAY, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
               Random random = new Random();
                int xf =random.nextInt(army.size);
                //int yf =;
                while(spcPnl.army.army.get(xf).isDestroyed)
                {
                    xf=random.nextInt(army.army.size());
                    if (lvlUP())
                    {
                       
                       break;
                    }
//                    if(!lvlUpFlag)
//                    {
//                        lvlUP();
//                    }
//                    if(lvlUpFlag)
//                    {
//                        //spawnWave();
//                        lvlUpFlag=
//                        break;
//                    } 
                        break;
                }
//                if(lvlUpFlag)
//                {
//                    spawnWave();
//                }
                  
                if(!(spcPnl.army.army.get(xf).isDestroyed))
                    enemAmmo.EnFire(xf, xf);
            }
        });
        enFireTimer.start();
        spcPnl.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if(e.getKeyCode()==KeyEvent.VK_UP)
                {
                    if(player2.getY()>0)
                        player2.setY(player2.getY()-Constants.WIDTH);
                }
                else if(e.getKeyCode()==KeyEvent.VK_DOWN)
                {
                    if(player2.getY()<(Constants.WIDTH*Constants.PHEIGHT)-140)
                        player2.setY(player2.getY()+Constants.WIDTH);                    
                }
                else if(e.getKeyCode()==KeyEvent.VK_RIGHT)
                {
                    if(player2.getX()<(Constants.WIDTH*Constants.PWIDTH)-95)    
                        player2.setX(player2.getX()+Constants.WIDTH);                    
                }
                else if(e.getKeyCode()==KeyEvent.VK_LEFT)
                {
                    if(player2.getX()>0)
                        player2.setX(player2.getX()-Constants.WIDTH);                    
                }
                else if(e.getKeyCode()==KeyEvent.VK_SPACE)
                {
                    if(!player2.isDestroyed)
                    {
                        fightAmmo2.fire1();
                        spaceFrame.pew();
                    }
                }
            }
});
        spcPnl.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent me) {
                super.mouseMoved(me);
                fighter.x=me.getX()-40;//To change body of generated methods, choose Tools | Templates.
                fighter.y=me.getY();//To change body of generated methods, choose Tools | Templates.
            }
            
});
        spcPnl.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                if(!fighter.isDestroyed)
                {
                    fightAmmo.fire();
                    spaceFrame.pew();
                }
            }
            
});
        
        spcPnl.setFocusable(true);
        
    }
    
  //enemy fighter collision check 
  public void efhascollision(int x , int y )
  {
    for (int i = 0 ;i<army.army.size();i++)
    {
        if (x>(army.army.get(i).getX())-80 && x<(army.army.get(i).getX())+80 && y>(army.army.get(i).getY())-60 && y<(army.army.get(i).getY())+60)
        {
            if(!(army.army.get(i).isDestroyed))
            {
                if(x==fighter.x)
                {
                fighter.im=null;
                fighter.isDestroyed=true;
            
                }
                if(x==player2.x)
                {
                    player2.im=null;
                    player2.isDestroyed=true;
                }
            }
        }
    }
  }
  
  //fighter's projectile and enemy collision 
  public void fpehascollision()
  {
      for (int i = 0 ;i< fightAmmo.af.size();i++)
      {
          for (int j =0;j<army.army.size();j++ )
          {
              if (fightAmmo.af.get(i).getX() >(army.army.get(j).getX()) && fightAmmo.af.get(i).getX() <(army.army.get(j).getX())+80 && fightAmmo.af.get(i).getY() >(army.army.get(j).getY()) &&  fightAmmo.af.get(i).getY() <(army.army.get(j).getY())+60)
              {
                  if(!(army.army.get(j).isDestroyed) && !(fightAmmo.af.get(i).isDestroyed)){
                  army.army.get(j).erase(spcPnl.getGraphics());
                  fightAmmo.af.get(i).erase(spcPnl.getGraphics());}
              }
          }
      }
      
  }
  public void epfhasCollision()
  {
      for(int i=0;i<enemAmmo.ep.size();i++)
      {
          if(enemAmmo.ep.get(i).getX()>Fighter.x-20 && (enemAmmo.ep.get(i).getX()) < Fighter.x+60 && enemAmmo.ep.get(i).getY()> (Fighter.y)-10 && (enemAmmo.ep.get(i).getY())+ 30 <(Fighter.y)+60)
          {
              fighter.im=null;
              fighter.isDestroyed=true;
          }
      }
  }
  public boolean lvlUP()
  {
      
          if (army.army.size()!=Army.numofDes)
          {
              lvlUpFlag=false;
              return false;
          }
          else{
                lvlUpFlag=true;
                return true;
          }
  }
    public void spawnWave()
    {
        if(lvlUpFlag)
        {
            lvlUpFlag=false;
            level++;
            spaceFrame.lvlSound();
            descendTimer.setDelay(Math.max(Constants.DESDELAY-(15*(level-1)),10));
            enFireTimer.setDelay(Math.max(Constants.FIRDELAY-(200*(level-1)),50));
            lvlnum.setText(""+level);
            for(int i=0;i<level;i++){
                
                for(int j=0;j<10;j++)
                 {
                      army.army.add(new Enemy(j*80,60*i));
                      army.army.get(j).draw(spcPnl.getGraphics());
                    }
                 }
        }
    }
//  public void spawnarmy()
//  {
//      army = new Army();
//      
//  }
    public void fpehascollision2()
  {
      for (int i = 0 ;i< fightAmmo2.af1.size();i++)
      {
          for (int j =0;j<army.army.size();j++ )
          {
              if (fightAmmo2.af1.get(i).getX() >(army.army.get(j).getX()) && fightAmmo2.af1.get(i).getX() <(army.army.get(j).getX())+80 && fightAmmo2.af1.get(i).getY() >(army.army.get(j).getY()) &&  fightAmmo2.af1.get(i).getY() <(army.army.get(j).getY())+60)
              {
                  if(!(army.army.get(j).isDestroyed) && !(fightAmmo2.af1.get(i).isDestroyed)){
                  army.army.get(j).erase(spcPnl.getGraphics());
                  fightAmmo2.af1.get(i).erase(spcPnl.getGraphics());}
              }
          }
      }
      
  }
    public void epfhasCollision2()
  {
      for(int i=0;i<enemAmmo.ep.size();i++)
      {
          if(enemAmmo.ep.get(i).getX()>Fighter2.x-20 && (enemAmmo.ep.get(i).getX()) < Fighter2.x+60 && enemAmmo.ep.get(i).getY()> (Fighter2.y)-10 && (enemAmmo.ep.get(i).getY())+ 30 <(Fighter2.y)+60)
          {
              player2.im=null;
              player2.isDestroyed=true;
          }
      }
  }
}
   

